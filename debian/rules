#!/usr/bin/make -f
include /usr/share/dpkg/buildopts.mk
DEB_BUILD_OPTION_PARALLEL ?= 1

%:
	dh $@ --builddirectory=carl9170-build

# The procedure for building the firmware was determined by
# examining autogen.sh. The build system doesn't cope well
# with us building in a subdirectory, so we need a few tricks
# to make it work.

.config execute_before_dh_auto_configure:
# This is where the build system expects to find the tools.
	mkdir -p toolchain/inst/bin
	cd toolchain/inst/bin && \
		for i in /usr/bin/sh-elf-*; \
		do ln -sf "$$i" `basename "$$i"`; \
		done
	if ! [ -d config-build ]; \
	then cp -r config config-build; \
	fi
	cd config-build && \
		cmake -G "Unix Makefiles" . && \
		make -j$(DEB_BUILD_OPTION_PARALLEL)
	config-build/conf --alldefconfig Kconfig

execute_before_dh_auto_build:
	for i in carlfw/carl9170.lds minifw/miniboot.lds tools/src/wol.c; \
	do \
		mkdir -p `dirname carl9170-build/"$$i"`; \
		cp "$$i" carl9170-build/"$$i"; \
	done

execute_after_dh_auto_build:
	carl9170-build/tools/src/miniboot a carl9170-build/carlfw/carl9170.fw carl9170-build/minifw/miniboot.fw

execute_before_dh_install:
	if [ -e carl9170-build/carlfw/carl9170.fw ]; \
	then mv carl9170-build/carlfw/carl9170.fw carl9170-build/carlfw/carl9170-1.fw; \
	fi

override_dh_gencontrol:
	for i in firmware-carl9170; \
	do \
		dh_gencontrol -p "$$i" -- -Vgcc-built-using="$(shell dpkg-query -Wf'$${Built-Using}\n' gcc-sh-elf | cut -d ',' -f 1)" \
			-Vnewlib-source-version=`dpkg-query -Wf'$${Built-Using-Newlib-Source}\n' libnewlib-sh-elf-dev | cut -d ':' -f 2`; \
	done	
