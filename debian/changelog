carl9170fw (1.9.9-450-gad1c721+dfsg-1) UNRELEASED; urgency=medium

  [ John Scott ]
  * New upstream version

  [ Ben Hutchings ]
  * d/salsa-ci.yml: Add CI configuration for salsa.debian.org
  * Exclude tools/src/eeprom_fix binary from upstream source
  * d/copyright: Add entry for upstream metainfo.xml

 -- John Scott <jscott@posteo.net>  Sun, 20 Aug 2023 12:39:56 -0400

carl9170fw (1.9.9-399-gcd480b9-1.2) unstable; urgency=medium

  * Non-maintainer upload
  * Move firmware to /usr hierarchy
  * Conflict on next firmware-linux-free revision (Closes: #1050989)

 -- Bastian Germann <bage@debian.org>  Wed, 05 Jun 2024 21:58:15 +0000

carl9170fw (1.9.9-399-gcd480b9-1.1) experimental; urgency=medium

  * Non-maintainer upload
  * Prevent executing dh_gencontrol on dropped udeb
  * Fix dual-licensed files with Atheros copyright
  * DEP-11: Add metainfo.xml with firmware provides (Closes: #1051266)

  [ John Scott ]
  * Static-Built-Using is no substitute for Built-Using
  * Change a makefile convention in debian/rules and drop the udeb package

 -- Bastian Germann <bage@debian.org>  Tue, 05 Sep 2023 13:45:59 +0000

carl9170fw (1.9.9-399-gcd480b9-1) experimental; urgency=medium

  * Initial release (Closes: #994625)

 -- John Scott <jscott@posteo.net>  Sat, 25 Dec 2021 12:23:08 -0500
